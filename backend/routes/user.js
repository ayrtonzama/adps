const express =require('express');
const router = express.Router();
const {create,deleteUser,getAll,getSingle,edit,optionsdata} =require('../database/mongoose/controllers/User')

router.route('/').post(create).get(getAll)
router.route('/options').get(optionsdata)
router.route('/:id').put(edit).get(getSingle).delete(deleteUser)
module.exports = router;