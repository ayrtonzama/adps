const express =require('express');
const router = express.Router();
const {login,logout,oneTimePin,registerUser} =require('../database/mongoose/controllers/Authentication')


router.route('/login').post(login);
router.route('/register').post(registerUser);
router.route('/logout').get(logout);
router.route('/otp').post(oneTimePin);


module.exports = router;