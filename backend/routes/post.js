const router = express.Router()

// router.post('/auth/login',(res,req,next)=>{

// })
// create
router.route('/post/create').post(create);

// get all
router.route('/posts/').get(getAll);

// get single delete single and update single
router.route('/post/:id').put(update).get(getSingle).delete(remove);


export default router;