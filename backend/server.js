const express = require('express')
const bodyParser = require('body-parser')
const dotenv=require('dotenv');
const Database =require('./database/connect.js')
dotenv.config({path:'./config/.env'})
const cors = require('cors')
const mongoose =require('mongoose')
const Authentication = require('./routes/authentication')
const User = require('./routes/user')
const morgan =require('morgan')
const {sessionCookie} = require('./services/secure')
const app = express()
const session = require('express-session');
const MongoStore = require('connect-mongo')(session);
const { uuid } = require('uuidv4');
app.use(bodyParser.json())
const PORT = process.env.PORT || 3000;
app.use(cors({
    credentials: true,
    origin: 'http://localhost:4200'
  }));
  
  
app.use(morgan('dev'));
app.use(session({
  name: "session",
  maxAge: 24 * 60 * 60 * 1000,
  secret: 'password',
  secure: true,
  httpOnly: true,
  signed: true,
  genid: () => {
      return uuid();
  },
  store:new MongoStore({mongooseConnection:mongoose.connection})
}))
// app.get('/',(req,res)=>{
//     console.log('hey')
// })

app.use('/auth',Authentication)
app.use('/user',User)

// Middleware


// Routes


app.listen(PORT, () =>{
 
    console.log('App started with cors enabled');
});