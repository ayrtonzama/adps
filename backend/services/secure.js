const session = require('express-session');
const MongoStore = require('connect-mongo')(session);
const { uuid } = require('uuidv4');
const mongoose =require('mongoose')
var jwt = require('jsonwebtoken');

const dotenv = require('dotenv');
dotenv.config({ path: '../config/.env' })
exports.sessionCookie = () => {
    
    console.log(process.env.SECRET_COOKIE_SESSION_KEY)
    return {
        name: "session",
        maxAge: 24 * 60 * 60 * 1000,
        secret: 'password',
        secure: true,
        httpOnly: true,
        signed: true,
        genid: () => {
            return uuid();
        },
        store:new MongoStore({mongooseConnection:mongoose.connection})
    }
}

exports.jwtSign= async(data)=>{
    var token= await jwt.sign(data,"password",{expiresIn:'24h'})
    return token
}

exports.jwtVerify=(token)=>{
    try{
        var verified=jwt.verify(token,"password")
        return verified;
    }catch(Exception){
        return null
    }
 
}