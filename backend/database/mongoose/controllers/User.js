const User = require('../schema/User')
const { jwtVerify } = require('../../../services/secure')
const Role = require('../schema/Role')
const Region = require('../schema/Region')
const Department = require('../schema/Department')
exports.create = (req, res, next) => {
   try {
      let user_detail = {
         first_name: req.body.first_name,
         last_name: req.body.last_name,
         email: req.body.email,
         phone_number: req.body.phone_number,
         password: req.body.password
      }
   } catch (Exception) {

   }
}

exports.deleteUser = (req, res, next) => {
   try {

   } catch (Exception) {

   }
}

exports.getAll = async (req, res, next) => {
   try {

      let token = breakBearer(req.header('Authorization'))

      let verify = jwtVerify(token);
      console.log(verify);
      let role = await Role.findById(verify.role._id);
      if (role.code == 'min') {
         User.find({}, (err, obj) => {
            let mappedData = obj.map(val => {
               return {
                  id: val._id,
                  first_name: val.first_name,
                  email: val.email,
                  last_name: val.last_name,
                  phone_number: val.phone_number,
                  // region:val.region,
                  // department:val.department,
                  is_verified: val.is_verified,
               }
            })
            res.status(200).send({ data: mappedData, status: "success" })
         })
      }
      // return res.status(200).send({data:verify,other:token})
   } catch (Exception) {
      console.log('here')
      return res.status(401).send({ data: Exception })

   }
}

exports.getSingle = async(req, res, next) => {
   try {

      let token = breakBearer(req.header('Authorization'))

      let verify = jwtVerify(token);
      console.log(verify);
      let role = await Role.findById(verify.role._id);
      if (role.code == 'min') {
         User.findById(req.params.id, async(err, obj) => {
            let roleSearch=await Role.findById(obj.role);
            let department=await Department.findById(obj.department);
            let region=await Region.findById(obj.region);
            let mappedData =  {
                  id: obj._id,
                  first_name: obj.first_name,
                  email: obj.email,
                  last_name: obj.last_name,
                  phone_number: obj.phone_number,
                  role:roleSearch?roleSearch._id:null,
                  department:department?department._id:null,
                  region:region?region._id:null,
                  is_verified: obj.is_verified,
               }
           
            res.status(200).send({ data: mappedData, status: "success" })
         })
      }
   } catch (Exception) {
      return res.status(401).send({ data: Exception })
   }
}

exports.edit = async(req, res, next) => {
   try {
      console.log(req.body)
      let token = breakBearer(req.header('Authorization'))

      let verify = jwtVerify(token);

      let role = await Role.findById(verify.role._id);
      if (role.code == 'min') {
         let userDetail=await User.findByIdAndUpdate(req.params.id,req.body);
         if(userDetail){
            return res.status(200).send({ status: "success" })
         }
      }
   } catch (Exception) {

   }
}

exports.optionsdata = async (req, res, next) => {
   try {
      console.log("heree")
      let roles = await Role.find();
      let regions = await Region.find();
      let departments = await Department.find();

      return res.status(200).send({ status: "success", departments: departments, regions: regions, roles: roles })
   } catch (Exception) {
      console.log(Exception)
      return res.status(401).send({ data: Exception })
   }
}

const breakBearer = (data) => {
   return data.substring(7, data.length)
}