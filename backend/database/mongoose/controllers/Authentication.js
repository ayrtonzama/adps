const User = require('../schema/User');
const Role = require('../schema/Role');
const bcrypt = require('bcryptjs');
const dotenv = require('dotenv');
const mongoose = require('mongoose');
const { verifySMSCreate, verifySms } = require('../../../services/smsBird.js')
const { jwtSign } = require('../../../services/secure')
dotenv.config({ path: '../../../config/.env' })
const Nexmo = require('nexmo');
// TODO require the bcrypt

exports.login = async (req, res, next) => {
    try {
        var pass = await bcrypt_helper(req.body.password);

        // Encrypt bcrypt
        let login_details = {
            email: req.body.email,
            password: pass
        }
        console.log(login_details);

        User.findOne({ email: req.body.email }, (err, obj) => {

            try {


                if (err) {
                    throw "Invalid Credentials"
                }
                console.log(obj.is_verified)
                if (obj !== null) {
                    if (bcrypt.compare(pass, obj.password)) {
                        // const resp = OTPStart();
                        if (!obj.is_verified) {

                            throw "Please wait for account to be verfied by an admin"
                        }

                        const resp = {
                            request_id: '71713cb7320f4179ad507cc7c075eeea',

                        }
                        console.log(resp)
                        if (resp) {

                            const temp = {
                                email: req.body.email,
                                token: resp.request_id,
                                status: "pending",

                            }
                            res.status(200).send(temp);
                        } else { throw "error occured" }
                    } else {
                        throw "Invalid Credentials"
                    }
                } else {
                    throw "Invalid Credentials"
                }
            } catch (error) {
                res.status(401).send({ error: error });
            }
            // OTPStart();

        })
        console.log('hey')
    } catch (Exception) {
        console.log("error occured", Exception);
        return res.status(401).send({ error: 'Error :' + Exception });
    }
}

exports.logout = (req, res, next) => {
    console.log("hello")
    req.session = null
    return res.status(201).send({data:"success"})
}
exports.registerUser = async (req, res, next) => {
    try {
        console.log(req.body)
        let pass = await bcrypt_helper(req.body.password);
        const user = new User(
            {
                first_name: req.body.first_name,
                last_name: req.body.last_name,
                email: req.body.email,
                phone_number: req.body.phone_number,
                password: pass
            });
        User.create(user, (err, obj) => {
            if (err) {
                throw err;
            }

            res.status(201).send({ data: "You  have been registered please wait for admin to verify.", status: "success" })
        });

    } catch (Exception) {
        console.log(Exception);
        return res.status(401).send('Error :' + Exception);
    }
}

exports.oneTimePin = async (req, res, next) => {
    try {

        if (req.body.otp) {
            var pass = await bcrypt_helper(req.body.password);

            // const resp = verifySms(req.body.token, req.body.otp)

            const resp = verifySmsSpoof(req.body.token, req.body.otp)
            if (!resp) {
                throw 'incorrect'
            }


            User.findOne({ email: req.body.email }, async (err, obj) => {
                console.log(obj)
                try {
                    console.log("here")

                    if (err) {
                        throw "Invalid Credentials"
                    }
                    if (obj !== null) {
                        let role = await Role.findById(obj.role);
                        if (bcrypt.compare(pass, obj.password)) {

                            let objectReturn = {
                                id: obj._id,
                                first_name: obj.first_name,
                                last_name: obj.last_name,
                                email: obj.email,
                                role: await role
                            }

                            let token = await jwtSign(objectReturn);
                            objectReturn.token = token;
                            res.status(200).send(objectReturn);
                        } else {
                            throw "Invalid Credentials"
                        }
                    } else {
                        throw "Invalid Credentials"
                    }
                } catch (Exception) {
                    console.log(Exception)
                }
                // OTPStart();

            })

            console.log('here')
        } else {
            throw 'incorrect token'
        }
    } catch (Exception) {
        console.log(Exception);
        return res.status(401).send('Error :' + Exception);
    }
}

exports.validationChecker = async (token) => {

}

const userRegister = (data) => {

    user.first_name = data.first_name;
}

const OTPStart = () => {
    const response = verifySMSCreate();
    return response;
}
const bcrypt_helper = async (password) => {
    var hashedItem = '';
    hashedItem = await bcrypt.hash(password, 10)
    return hashedItem;

}


const cancelVerify = () => {
    const nexmo = new Nexmo({
        apiKey: '048766cc',
        apiSecret: 'nKs7k3LEiGijWUrA',
    });
    nexmo.verify.control({
        request_id: '71713cb7320f4179ad507cc7c075eeea',
        cmd: 'cancel'
    }, (err, result) => {
        console.log(err ? err : result)
    });
}

const verifySmsSpoof = (token, otp) => {
    if (token == '71713cb7320f4179ad507cc7c075eeea') {
        if (otp == 9647) {
            return true
        } else {
            return false
        }
    } else {
        return false
    }
}

