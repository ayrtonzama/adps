/**
 * Make any changes you need to make to the database here
 */
const mongoose = require('mongoose');
const RoleSchema =require('../schema/Role');
async function up () {
  // Write migration here
  await RoleSchema.create({
    name:"ministers",
    code:"MIN"
  });
  await RoleSchema.create({
    name:"premieres",
    code:"pre"
  });
  await RoleSchema.create({
    name:"clerks",
    code:"clk"
  });
}

/**
 * Make any changes that UNDO the up function side effects here (if possible)
 */
async function down () {
  // Write migration here
}

module.exports = { up, down };
