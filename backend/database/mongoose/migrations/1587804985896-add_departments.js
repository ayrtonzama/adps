/**
 * Make any changes you need to make to the database here
 */
const mongoose = require('mongoose');
const DepartmentSchema = require('../schema/Department');
async function up() {
  // Write migration here
  await DepartmentSchema.create({
    name: "Agriculture and Water and Sanitation departments",
    code: "AWSD"
  });
}

/**
 * Make any changes that UNDO the up function side effects here (if possible)
 */
async function down() {
  // Write migration here
}

module.exports = { up, down };
