const mongoose = require('mongoose');
const User = require('./User');
const Region = require('./Region');
const Department = require('./Department');
var Schema = mongoose.Schema;

const PostModelSchema=new Schema({
    title:String,
    content:String,
    created_at:String,
    deleted_at:String,
    owner:{type: mongoose.Schema.Types.ObjectId, ref: "User"},
    type:{type: mongoose.Schema.Types.ObjectId, ref: "Department"},
    region:{type: mongoose.Schema.Types.ObjectId, ref: "Region"}
});

module.exports = mongoose.model("Post", PostModelSchema);