const mongoose = require('mongoose');

var Schema = mongoose.Schema;

const DepartmentModelSchema = new Schema({

    name: String,
    code: String,

});

module.exports = mongoose.model("Department", DepartmentModelSchema);