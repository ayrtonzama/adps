const mongoose = require('mongoose');
const Role = require('./Role')
const Department = require('./Department')
const Region = require('./Region')
const Post = require('./Post')
var Schema = mongoose.Schema;

const UserModelSchema=new Schema({
    first_name:String,
    last_name:String,
    email:String,
    phone_number:String,
    password:String,
    is_verified:{type:Boolean,default:false},
    role: {type:Schema.Types.ObjectId, ref:Role},
    department: {type: Schema.Types.ObjectId, ref: "Department"},
    region: {type: Schema.Types.ObjectId, ref: "Region"},
    posts: [{type: Schema.Types.ObjectId, ref: "Post"}]
    // region:Region,
    // posts:[Post]
});

module.exports = mongoose.model("User", UserModelSchema);