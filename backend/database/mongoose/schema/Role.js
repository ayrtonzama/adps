const mongoose = require('mongoose');

var Schema = mongoose.Schema;

const RoleModelSchema = new Schema({

    name: String,
    code: String,

});

module.exports = mongoose.model("role", RoleModelSchema);