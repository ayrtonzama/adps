const mongoose = require('mongoose');
const dotenv = require('dotenv');

dotenv.config({ path: '../config/.env' });

class Database {
    constructor() {
        this._connect();
    }

    _connect() {

        mongoose.connect(`mongodb://mongodb_container:27017/apds_db`)
            .then(() => {
                console.log('Successful Connection');
            }).catch(err => {
                console.log(`mongodb://${process.env.MONGODB_URL}/${process.env.MONGODB_NAME}`)
                console.error('Database connection error ' + err)
            })
    }

}

module.exports = new Database();