import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import {HttpClientModule} from '@angular/common/http';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { LoginComponent } from './screens/Authentication/login/login.component';
import { RegisterComponent } from './screens/Authentication/register/register.component';
import { NavbarComponent } from './components/layouts/navbar/navbar.component';
import { FooterComponent } from './components/layouts/footer/footer.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import {MatCheckboxModule} from '@angular/material/checkbox';
import {MatToolbarModule} from '@angular/material/toolbar';
import {MatGridListModule} from '@angular/material/grid-list';
import {MatCardModule} from '@angular/material/card';
import {MatFormFieldModule} from '@angular/material/form-field';
import { MatInputModule } from '@angular/material';
import {MatButtonModule} from '@angular/material/button';
import {MatDialogModule} from '@angular/material/dialog';
import {MatMenuModule} from '@angular/material/menu';
import {MatTableModule} from '@angular/material/table';
import {MatSelectModule} from '@angular/material/select';

import { DashboardComponent } from './screens/dashboard/dashboard.component';
import { OtpDialogComponent } from './components/otp-dialog/otp-dialog.component';
import { UserIndexComponent } from './screens/user/user-index/user-index.component';
import { UserChangeComponent } from './screens/user/user-change/user-change.component';
@NgModule({
  declarations: [
    AppComponent,
    LoginComponent,
    RegisterComponent,
    NavbarComponent,
    FooterComponent,
    DashboardComponent,
    OtpDialogComponent,
    UserIndexComponent,
    UserChangeComponent,
    
  ],
  imports: [HttpClientModule,
    BrowserModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    MatToolbarModule,
    MatGridListModule,
    MatCardModule,
    MatFormFieldModule,ReactiveFormsModule,
    MatInputModule,
    MatButtonModule,MatDialogModule,
    MatMenuModule,MatTableModule,MatCheckboxModule,MatSelectModule
  ],
  entryComponents:[OtpDialogComponent],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
