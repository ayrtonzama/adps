import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable, of } from 'rxjs';
import { environment } from 'src/environments/environment';
import { Role } from '../Models/Role';

@Injectable({
    providedIn: 'root',
})
export class RoleService {

    headerOptions = {
        headers: new HttpHeaders({ 'Content-Type': 'application/json' }),
        withCredentials: true
    }
    constructor(private http: HttpClient) {

    }
    getRoles() {

    }
    getRole() {

    }
    postRole() {

    }
    updateRole() {

    }
    deleteRole() {

    }
   
}