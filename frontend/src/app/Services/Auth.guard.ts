import { Injectable } from '@angular/core';
import { CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot } from '@angular/router';


@Injectable({
    providedIn: 'root'
})
export class AuthGuard implements CanActivate {
    canActivate(
        next: ActivatedRouteSnapshot,
        state: RouterStateSnapshot): boolean {
        return this.checkUserLoggedIn()
    }

    checkUserLoggedIn():boolean{
        let token =localStorage.getItem('token')
        // TODO check token validation
        if(token){
            
            return true;
        }
        return false;
    }


}