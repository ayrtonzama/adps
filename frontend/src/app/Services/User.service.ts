import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable, of } from 'rxjs';
import { environment } from 'src/environments/environment';
import { User } from '../Models/User';

@Injectable({
    providedIn: 'root',
})
export class UserService {

    headerOptions = {
        headers: new HttpHeaders({ 'Content-Type': 'application/json' ,'Authorization':'Bearer '}),
        withCredentials: true
    }
    constructor(private http: HttpClient) {

    }
    getUsers() {
        let header=new HttpHeaders(
            {
                'Content-Type':'application/json',
                'Authorization':`Bearer ${localStorage.getItem('token')}`
            }
        )

        return this.http.get(`${environment.backend_url}${environment.user_header}/`,{headers:header,withCredentials:true});
    }
    getUser(id:string) {
        let header=new HttpHeaders(
            {
                'Content-Type':'application/json',
                'Authorization':`Bearer ${localStorage.getItem('token')}`
            }
        )
        return this.http.get(`${environment.backend_url}${environment.user_header}/${id}`,{headers:header})
    }
    postUser() {

    }
    updateUser(id:string,body:any) {
        let header=new HttpHeaders(
            {
                'Content-Type':'application/json',
                'Authorization':`Bearer ${localStorage.getItem('token')}`
            }
        )
        return this.http.put(`${environment.backend_url}${environment.user_header}/${id}`,body,{headers:header})
    }
    deleteUser() {

    }
    activateUser() {

    }
    registerUser(data){
        let body={
            first_name:data.first_name,
            last_name:data.last_name,
            email:data.email,
            password:data.password,
            phone_number:data.phone_number
        };
        return this.http.post(`${environment.backend_url}${environment.register_url}`,body,this.headerOptions)
    }
}