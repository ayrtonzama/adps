import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable, of } from 'rxjs';
import { environment } from 'src/environments/environment';
import { Region } from '../Models/Region';

@Injectable({
    providedIn: 'root',
})
export class RegionService {

    headerOptions = {
        headers: new HttpHeaders({ 'Content-Type': 'application/json' }),
        withCredentials: true
    }
    constructor(private http: HttpClient) {

    }
    getRegions() {

    }
    getRegion() {

    }
    postRegion() {

    }
    updateRegion() {

    }
    deleteRegion() {

    }
   
}