import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable, of } from 'rxjs';
import { environment } from 'src/environments/environment';
import { Post } from '../Models/Post';

@Injectable({
    providedIn: 'root',
})
export class PostService {

    headerOptions = {
        headers: new HttpHeaders({ 'Content-Type': 'application/json' }),
        withCredentials: true
    }
    constructor(private http: HttpClient) {

    }
    getPosts() {

    }
    getPost() {

    }
    postPost() {

    }
    updatePost() {

    }
    deletePost() {

    }
   
}