import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';

import { environment } from 'src/environments/environment';


@Injectable({
    providedIn: 'root',
})
export class optionService {

    headerOptions = {
        headers: new HttpHeaders({ 'Content-Type': 'application/json' ,'Authorization':'Bearer '}),
        withCredentials: true
    }
    constructor(private http: HttpClient) {

    }
    getUserOptions(){
        let header=new HttpHeaders(
            {
                'Content-Type':'application/json',
                'Authorization':`Bearer ${localStorage.getItem('token')}`
            }
        )

        return this.http.get(`${environment.backend_url}${environment.user_header}${environment.options}`,{headers:header});
    }
}