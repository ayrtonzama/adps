import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable, of } from 'rxjs';
import { environment } from 'src/environments/environment';
import { Department } from '../Models/Department';

@Injectable({
    providedIn: 'root',
})
export class DepartmentService {

    headerOptions = {
        headers: new HttpHeaders({ 'Content-Type': 'application/json' }),
        withCredentials: true
    }
    constructor(private http: HttpClient) {

    }
    getDepartments() {

    }
    getDepartment() {

    }
    postDepartment() {

    }
    updateDepartment() {

    }
    deleteDepartment() {

    }

}