import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Subject, BehaviorSubject } from 'rxjs';
import { environment } from 'src/environments/environment';
import { User } from '../Models/User';
import { Router } from '@angular/router';
import { Role } from '../Models/Role';

@Injectable({
    providedIn: 'root',
})
export class AuthService {

    bs: BehaviorSubject<User>;
    headerOptions = {
        headers: new HttpHeaders({ 'Content-Type': 'application/json' }),
        withCredentials: true
    }
    constructor(private http: HttpClient, private router: Router) {
        this.bs = new BehaviorSubject(null);
    }
    checkCookie(): boolean {
        return true;

    }

    loginUser(loginDetails) {
        // Remove the headers not needed

        return this.http.post(`${environment.backend_url}${environment.login_url}`, loginDetails, this.headerOptions);

    }

    oneTimePin(pin) {
  
        const body = {
            otp: pin.otp,
            token: localStorage.getItem('token_api'),
            email: pin.user.email,
            password: pin.user.password
        }
       
        return this.http.post(`${environment.backend_url}${environment.otp_url}`, body, this.headerOptions).subscribe((succ: { id: any, first_name: string, token: string, last_name: string, role: { _id: any, name: string, code: string }, email: string, phone_number: string }) => {
            this.setUser(succ);

        });
    }

    logoutUser() {

        return this.http.get(`${environment.backend_url}${environment.logout_url}`, 
        this.headerOptions).subscribe(succ=>{
            if(succ){
              this.bs.next(null)
              localStorage.clear();
              this.router.navigate(['auth/login'])
            }
          });;
    }

    registerUser(UserDetails) {
    
        return this.http.post(`${environment.backend_url}${environment.register_url}`, UserDetails, this.headerOptions);
    }

    getUser() {
        if(localStorage.getItem('activeUser')){
            this.bs.next(Object.assign(new User(),JSON.parse(localStorage.getItem('activeUser'))));
        }else{
            this.bs.next(null);
        }
    }
    setUser(data: { id: any, first_name: string, token: string, last_name: string, role: { _id: any, name: string, code: string }, email: string, phone_number: string }) {
        localStorage.setItem("token", data.token);

        // TODO format data
        let user = new User()
        let role = new Role();
        role = {
            id: data.role._id, name: data.role.name, code: data.role.code
        };
        user = {
            id: data.id,
            first_name: data.first_name,
            last_name: data.last_name,
            email: data.email,
            role: role,
            phone_number: data.phone_number,
            department: null,
            region: null,
            posts: []
        }

        localStorage.setItem("activeUser",JSON.stringify(user))
        this.bs.next(user)
        this.router.navigate(['/dashboard'])
    }

    currentUser$(){
        return this.bs;
    }

    isAuthenticated(){
        if(this.bs.value){
            return true
        }else{
            return false;
        }
    }
}