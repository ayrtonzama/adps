export class Department {
    id: String;
    name: String;
    code: String;
    constructor(
        id: String = '',
        name: String = '',
        code: String = ''
    ) {
        this.id = id;
        this.name = name;
        this.code = code;
    }
}