import { Department } from './Department';
import { Region } from './Region';
import { User } from './User';

export class Post{
    id:String;
    title:String;
    content:String;
    type:Department;
    region:Region;
    created_at:String;
    deleted_at:String;
    owner:User;
    constructor(){

    }
}