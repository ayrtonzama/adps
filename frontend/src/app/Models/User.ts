import { Role } from './Role';
import { Department } from './Department';
import { Region } from './Region';
import { Post } from './Post';
export class User {
    id: String;
    first_name: String;
    last_name: String;
    email: String;
    phone_number: String;
    role: Role;
    department: Department;
    region: Region
    posts:Array<Post>
    constructor(

        id: String = '',
        first_name: String = '',
        last_name: String = '',
        email: String = '',
        phone_number: String = '',
        role: Role = null,
        department: Department = null,
        region: Region = null
    ) {
        this.id = id;
        this.first_name = first_name;
        this.last_name = last_name;
        this.email = email;
        this.phone_number = phone_number;
        this.role = role;
        this.department = department;
        this.region = region;
    }
}