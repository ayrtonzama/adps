import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { LoginComponent } from './screens/Authentication/login/login.component';
import { RegisterComponent } from './screens/Authentication/register/register.component';
import { DashboardComponent } from './screens/dashboard/dashboard.component';
import {UserIndexComponent} from './screens/user/user-index/user-index.component';
import {UserChangeComponent} from './screens/user/user-change/user-change.component';
import { AuthGuard } from './Services/Auth.guard';
const routes: Routes = [
  {
    path: 'auth/login',
    component: LoginComponent
  },
  {
    path: 'auth/register',
    component: RegisterComponent
  }, {
    path: 'dashboard',
    component: DashboardComponent,
    canActivate: [AuthGuard]
  }
  ,{
    path: 'user/index',
    component: UserIndexComponent,
    canActivate: [AuthGuard] 
  }
  ,{
    path: 'user/:id',
    component: UserChangeComponent,
    canActivate: [AuthGuard] 
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
