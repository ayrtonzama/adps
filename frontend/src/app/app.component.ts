import { Component, OnInit } from '@angular/core';
import { AuthService } from './Services/Auth.service';
import { User } from './Models/User';
import { Subscription } from 'rxjs';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit {
  title = 'frontend';
  currentUser: User = null;
  UserSubscription:Subscription;
  constructor(private auth: AuthService) {

  }
  ngOnInit(): void {
    this.auth.getUser();
    this.UserSubscription=this.auth.currentUser$().subscribe((user)=>{
      this.currentUser=user
    })
  }
}
