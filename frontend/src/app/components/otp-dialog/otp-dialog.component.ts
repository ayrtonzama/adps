import { Component, OnInit,Input,Inject } from '@angular/core';
import { Router } from '@angular/router';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { AuthService } from 'src/app/Services/Auth.service';
import { MAT_DIALOG_DATA,MatDialogRef} from '@angular/material/dialog';
export interface DialogData {
  data:{email:string,password:string};
}

@Component({
  selector: 'app-otp-dialog',
  templateUrl: './otp-dialog.component.html',
  styleUrls: ['./otp-dialog.component.scss']
})

export class OtpDialogComponent implements OnInit {
  OTPForm: FormGroup;
  constructor(public dialog: MatDialogRef<'app-otp-dialog'>,private auth: AuthService,@Inject(MAT_DIALOG_DATA) public login:DialogData) { }

  ngOnInit() {
    this.OTPForm = this.generateOTPForm();
  }

  generateOTPForm(): FormGroup {
    let otp = {
      'otp': new FormControl('', [Validators.required])
    };
    return new FormGroup(otp, { updateOn: 'submit' })
  }

  otpSubmit():void{
    if(this.OTPForm.valid){
      let obj={otp:this.OTPForm.value.otp,user:this.login};
      this.auth.oneTimePin(obj)
      this.dialog.close();
    }
  }
}
