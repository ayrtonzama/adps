import { Component, OnInit, Input } from '@angular/core';
import { AuthService } from 'src/app/Services/Auth.service';

@Component({
  selector: 'app-navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.scss']
})
export class NavbarComponent implements OnInit {
  @Input() currentUser;
  constructor(private auth:AuthService) { }

  ngOnInit() {
  }

  logout():void{
    this.auth.logoutUser()
  }

}
