import { Component, OnInit } from '@angular/core';
import {FormControl,FormGroup, Validators} from '@angular/forms';
import {AuthService} from '../../../Services/Auth.service';
@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.scss']
})
export class RegisterComponent implements OnInit {
RegisterForm:FormGroup;
  constructor(private authService:AuthService) { }

  ngOnInit() {
    this.RegisterForm=this.generateForm();
  }

  generateForm(){
    let register={
      'first_name':new FormControl('',[Validators.required,Validators.minLength(1)]),
      'last_name':new FormControl('',[Validators.required,Validators.minLength(1)]),
      'phone_number':new FormControl('',[Validators.required,Validators.maxLength(10)]),
      'email':new FormControl('',[Validators.required,Validators.email]),
      'password':new FormControl('',[Validators.required]),
      'confirm_password':new FormControl('',[Validators.required])
    }
    return new FormGroup(register,{updateOn:'submit'});
  }
  submit():void{
    
    if(this.RegisterForm.valid){
      if(this.RegisterForm.value.password==this.RegisterForm.value.confirm_password){

      }
      let obj ={
        first_name:this.RegisterForm.value.first_name,
        last_name:this.RegisterForm.value.last_name,
        phone_number:this.RegisterForm.value.phone_number,
        email:this.RegisterForm.value.email,
        password:this.RegisterForm.value.password
      };
      this.authService.registerUser(obj).subscribe((resp)=>{
      },err=>{

      })
    }
  }
}
