import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { AuthService } from 'src/app/Services/Auth.service';
import { OtpDialogComponent } from 'src/app/components/otp-dialog/otp-dialog.component';
import { MatDialog } from '@angular/material';
import { Input } from '@angular/core';
@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {
  LoginForm: FormGroup;
  OTPForm: FormGroup;
  
  constructor(private auth: AuthService, public dialog: MatDialog) { }

  ngOnInit() {
    this.LoginForm = this.generateForm();
    this.OTPForm = this.generateOTPForm();
  }
  generateOTPForm(): FormGroup {
    let otp = {
      'otp': new FormControl('', [Validators.required])
    };
    return new FormGroup(otp, { updateOn: 'submit' })
  }
  generateForm(): FormGroup {
    let login = {
      'email': new FormControl('', [Validators.required, Validators.email]),
      'password': new FormControl('', [Validators.required])
    }
    return new FormGroup(login, { updateOn: 'submit' });
  }

  submit(): void {
    if(this.LoginForm.valid){

      let obj = { email: this.LoginForm.value.email, password: this.LoginForm.value.password }
      this.auth.loginUser(obj)
      
      .subscribe((resp: { token: String, status: String }) => {
        if (resp.token&&resp.status=='pending') {
          localStorage.setItem('token_api', resp.token.toString());
          let dialog = this.dialog.open(OtpDialogComponent, { width: "250px", height: "250px",data:{...obj} });
          dialog.afterClosed().subscribe(result => {
            
          });
        }
      }, err => {
        this.LoginForm.controls["email"].errors["Please check field is valid"]
        this.LoginForm.controls["password"].errors["Please check field is valid"]
      })
    }



  }

}
