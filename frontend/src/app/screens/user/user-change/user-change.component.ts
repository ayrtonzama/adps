import { Component, OnInit } from '@angular/core';
import { UserService } from 'src/app/Services/User.service';
import { Router, ActivatedRoute } from '@angular/router';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { optionService } from 'src/app/Services/option.service';

@Component({
  selector: 'app-user-change',
  templateUrl: './user-change.component.html',
  styleUrls: ['./user-change.component.scss']
})
export class UserChangeComponent implements OnInit {
  user_id: string;
  userForm: FormGroup;
  type: string;
  roles:Array<{_id:String,code:String,name:String}>
  constructor(private userService: UserService, private route: ActivatedRoute,private optionServer:optionService) { }

  ngOnInit() {
    this.route.params.subscribe(params=>{

      this.user_id=params['id']
    })
    this.optionServer.getUserOptions().subscribe((succ:{status:string,roles:Array<{_id:String,code:String,name:String}>,departments:Array<{_id:String,code:String,name:String}>,regions:Array<{_id:String,code:String,name:String}>})=>{

      this.roles=succ.roles
    });
      this.userService.getUser(this.user_id).subscribe((succ: {data:{
        email: string,
        first_name: string,
        last_name: string,
        phone_number: string,
        is_verified: boolean,
        role: string,
        region: string,
        department: string
      },status:string}) => {

        let user = {
          'email': new FormControl(succ.data.email, [Validators.required]),
          'first_name': new FormControl(succ.data.first_name, [Validators.required, Validators.minLength(1)]),
          'last_name': new FormControl(succ.data.last_name, [Validators.required, Validators.minLength(1)]),
          'phone_number': new FormControl(succ.data.phone_number, [Validators.required, Validators.minLength(1)]),
          'is_verified': new FormControl(succ.data.is_verified),
          // 'department': new FormControl(succ.data.department, [Validators.required,]),
          'role': new FormControl(succ.data.role, [Validators.required,]),
          // 'region': new FormControl(succ.data.region, [Validators.required,])
        }

        this.userForm = new FormGroup(user, { updateOn: 'submit' })
      })

    

  }

  submit(): void {
  
    if (this.userForm.valid) {
      let obj={
        first_name:this.userForm.value.first_name,
        last_name:this.userForm.value.last_name,
        phone_number:this.userForm.value.phone_number,
        role:this.userForm.value.role,
        is_verified:this.userForm.value.is_verified
      }

      this.userService.updateUser(this.user_id,obj).subscribe(succ=>{
        
      })
    }
  }

}
