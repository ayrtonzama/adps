import { Component, OnInit } from '@angular/core';
import { AuthService } from 'src/app/Services/Auth.service';
import { UserService } from 'src/app/Services/User.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-user-index',
  templateUrl: './user-index.component.html',
  styleUrls: ['./user-index.component.scss']
})
export class UserIndexComponent implements OnInit {
  dataTable:Array<{}>
  dataName:string[]=["first_name", "last_name","email","phone_number","is_verified","action"];
  constructor(private auth:AuthService,private userService:UserService,private route:Router) { }

  ngOnInit() {
    this.userService.getUsers().subscribe((succ:{
      data:Array<{first_name:string,
        last_name:string,
        email:string,
        id:string,
        phone_number:string,
        is_verified:boolean}>,
      status:string})=>{
      this.dataTable=succ.data.map(item=>{
        return {
          id:item.id,
          first_name:item.first_name,
          last_name:item.last_name,
          email:item.email,
          phone_number:item.phone_number,
          is_verified:item.is_verified,
        }
      });
    })
  }

  

}
